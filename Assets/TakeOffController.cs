using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class TakeOffController : MonoBehaviour
{
    private bool engineOn = false;
    public ARSessionOrigin ARSessionOriginReference;
    private ZeroController ZeroControllerReference;

    // Function to use in button onClick event
    public void OnClick()
    {
        if (ZeroControllerReference == null) ZeroControllerReference = ARSessionOriginReference.GetComponentInChildren<ZeroController>();
        GameObject airplaneGameObject = GameObject.FindWithTag("AirplanePrefab");
        if (airplaneGameObject != null)
        {
            Animator airplaneAnimator = airplaneGameObject.GetComponent<Animator>();
            if (!engineOn)
            {
                airplaneAnimator.SetTrigger("engineStart");
                Button takeOffButton = gameObject.GetComponentInChildren<Button>();
                if (takeOffButton != null) takeOffButton.GetComponentInChildren<Text>().text = "Take Off";
                engineOn = true;
            }
            else
            {
                airplaneAnimator.SetTrigger("takeOffStart");
                Invoke(nameof(StartDestroyCountDown), 1.0f);
            }
        }
    }

    private void StartDestroyCountDown()
    {
        if (gameObject != null)
        {
            ZeroControllerReference.SetTookOffTrue();
            Destroy(gameObject);
        }
    }
}

