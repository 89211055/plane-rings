using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class PrefabsCreator : MonoBehaviour
{
    [SerializeField] private GameObject airplanePrefab;
    [SerializeField] private GameObject ringPrefab;
    private ARTrackedImageManager aRTrackedImageManager;
    private GameObject airplane;

    private void OnEnable()
    {
        aRTrackedImageManager = gameObject.GetComponent<ARTrackedImageManager>();
        aRTrackedImageManager.trackedImagesChanged += OnImageChanged;
    }

    private void OnImageChanged(ARTrackedImagesChangedEventArgs obj)
    {
        foreach (ARTrackedImage image in obj.added)
        {
            // Render the airplane on top of the target AR tracked image
            airplane = Instantiate(airplanePrefab, image.transform);
            InitRings(image.transform);
        }
    }

    private void InitRings(Transform imageTransform)
    {
        for (int i = 0; i < 10; i++)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-2f, 2f), 0.1f, Random.Range(-2f, 2f));
            //Vector3 position = Random.insideUnitSphere * 1 + imageTransform.position;
            //position.y = imageTransform.position.y;
            //spawnPosition.y = imageTransform.position.y;
            Instantiate(ringPrefab, spawnPosition, Quaternion.identity);
        }
    }
}
