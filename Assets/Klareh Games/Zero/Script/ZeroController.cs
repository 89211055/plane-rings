using UnityEngine;

public class ZeroController : MonoBehaviour
{
    private FixedJoystick steerJoystick;
    private FixedJoystick heightJoystick;
    private Rigidbody rigidBody;
    // Boolean to keep the state if the airplane departured(took off)
    private bool airplaneTookOff = false;
    // Set airplaneTookOff to true(set from TakeOffController after the animation takeOff)
    public void SetTookOffTrue()
    {
        airplaneTookOff = true;
    }

    private void OnEnable()
    {
        GameObject steerJoystickObject = GameObject.FindWithTag("SteerJoystick");
        steerJoystick = steerJoystickObject.GetComponent<FixedJoystick>();
        GameObject heightJoystickObject = GameObject.FindWithTag("HeightJoystick");
        heightJoystick = heightJoystickObject.GetComponent<FixedJoystick>();
        // Get the rigidBody of this gameObject
        rigidBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (airplaneTookOff)
        {
            // Move the airplane forward every frame if it took off
            rigidBody.AddForce(10f * Time.deltaTime * transform.forward, ForceMode.VelocityChange);

            // Steer the airplane
            float xVal = steerJoystick.Vertical; // Get vertical instead of horizontal, due to the joystick placement on the canvas(Rotation won't help to make it right)
            xVal *= -1f; // We must mutliply with -1, to invert the already inverted Vertical, so we can get the correct value
            float rotationAmount = xVal * 100f * Time.deltaTime;
            transform.Rotate(Vector3.up, rotationAmount);

            // Change airplane height
            float yVal = heightJoystick.Horizontal;
            Vector3 upwardForce = new Vector3(0, yVal * 10f, 0);
            rigidBody.AddForce(upwardForce * Time.deltaTime, ForceMode.Impulse);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("trigger");
        DestroyImmediate(other.gameObject);
    }

}
